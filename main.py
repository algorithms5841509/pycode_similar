import ast
import re
import sqlite3
from difflib import SequenceMatcher
import os

import detectors.fingerprint
from detectors import fingerprint
from elasticsearch import Elasticsearch
import shutil
from detectors import fingerprint as finger
from tokenizer import tokenizer_Py as token
from elasticsearch_dsl import Document, connections, Search, Text, Keyword
from fuzzywuzzy import fuzz
import click

# HOST = 'https://0.0.0.0:9200'
# PORT = 9200
es = connections.create_connection(hosts=["http://elastic:password@elasticsearch:9200"])
# HOST = 'elasticsearch'
# PORT = 9200
# es = connections.create_connection(hosts="http://elasticsearch:9200")
# es = Elasticsearch("http://0.0.0.0:9200")

K = 10
T = 12
NAME_IN = 'pysimilar'
TOKENS_JSON = './tokens/tokens_Python.json'


def delete_comments(String):
    # import_pattern = re.compile(
    #     r'(?<!from)import (\w+)[\n.]|from\s+(\w+)\s+import')
    comment_pattern = re.compile(r'#.*?$|''(?:\'\'\'[\s\S]*?\'\'\'|"""[\s\S]*?""")', re.MULTILINE)
    new_file = comment_pattern.sub('', String)
    import_pattern = re.compile(
        r'(?:(?:(?:import|from)\s+[a-zA-Z_][a-zA-Z0-9_.]*\s+import.*)|(?:import\s+[a-zA-Z_][a-zA-Z0-9_]*\s*.))$',
        re.MULTILINE)

    new_new_file = import_pattern.sub('', new_file)
    nn = new_new_file.replace('\n\n', '')
    return nn


def find_variables(String):
    variable_pattern = re.compile(r'\b[a-zA-Z_][a-zA-Z0-9_]*\b')

    new = variable_pattern.sub('', String)
    return new


def Sample(filename):
    with open(filename, 'r', encoding='utf-8') as f:
        text = f.readlines()

    tokens = token.tokenizer(filename, TOKENS_JSON)
    tokenstring = ''.join([x[0] for x in tokens])
    fingerprints = finger.fingerprints(tokens, K, T)
    return tokenstring


class Article(Document):
    index_name = Keyword()

    docname = Keyword()
    author = Keyword()
    subject = Keyword()

    text = Text()
    tokens = Text()
    tokenstring = Text()
    fingerprints = Text()
    fpp = Text()

    class Index:
        name = NAME_IN
        settings = {
            "number_of_shards": 5,
            "number_of_replicas": 5,
        }


def find_python_files(directory):
    """
    Functions to find all python files in a directory
    :param directory:
    :return: python_files
    """
    python_files = []
    for root, _, files in os.walk(directory):
        if root != directory:
            if "/venv/" not in root:
                relative_path = os.path.relpath(root, directory)
                with open("./sour/" + relative_path.split("/")[0], 'a') as outfile:
                    for file in files:
                        if file.endswith('.py') and file != '__init__.py':
                            if file == outfile:
                                continue
                            next_file = root + "/" + file
                            with open(next_file, 'r+') as readfile:
                                shutil.copyfileobj(readfile, outfile)
                                # outfile.write(readfile.read())
                                # python_files.append(
                                #     (relative_path.split('/')[0], root + "/" + file, load_file(root + "/" + file), file))
    # return python_files
    return outfile


import os


def remove_non_python_files(directory):
    for root, dirs, files in os.walk(directory):
        for file in files:
            if not file.endswith('.py'):
                file_path = os.path.join(root, file)
                os.remove(file_path)
                print(f'Removed non-Python file: {file_path}')


def get_name_file(file):
    return file.split("/")[-1]


def load_file(file_path):
    """
    Loads a file and read it
    :param file_path:
    :return: file content
    """
    with open(file_path, "r") as f:
        return f.read()


def extract_code_from_python_file(file_path):
    """
    Функция убирает все лишнее описание и оставляет только код
    :param file_path:
    :return: File without dockstrings
    """
    with open(file_path, "r", encoding="utf-8") as file:
        code = file.read()
        parsed = ast.parse(code)

        code_lines = []
        for node in parsed.body:
            if not isinstance(node, (ast.FunctionDef, ast.ClassDef, ast.Import, ast.ImportFrom, ast.Expr)):
                start_lineno = getattr(node, "lineno", None)
                if start_lineno:
                    code_lines.extend(code.splitlines()[start_lineno - 1:])

        return "\n".join(code_lines)


class Start(object):
    @staticmethod
    def create():
        """
            Создание пустого индекса с именем name_in
        """
        if es.indices.exists(index=NAME_IN):
            es.indices.delete(index=NAME_IN)
        Article.init()
        new_index = Article()
        new_index.save()
        print("Index created!")

    @staticmethod
    def add(filename):
        """
            Добавление в базу
        """

        with open(filename, 'r', encoding='utf-8') as f:
            text = f.readlines()

        tokens = token.tokenizer(filename, TOKENS_JSON)
        tokenstring = ''.join([x[0] for x in tokens])
        fp = finger.ngramss(tokenstring, 5)
        try:
            fingerprints = finger.fingerprints(tokens, K, T)
        except ValueError:
            return -1
        buf = filename.split('/')
        docname = filename
        buf = docname.split('/')
        # if len(buf) != 4:
        #     print(filename, " - filename error")
        #     exit()
        # author = buf[3]
        # subject = buf[1]
        # work_type = buf[2]
        # task_num = buf[3].split('.')[0]
        article = Article()
        article.index_name = NAME_IN
        article.docname = docname
        # article.author = author

        result = Search(using=es, index=NAME_IN) \
            .query("match", docname=docname)
        response = result.execute()
        if response.hits.total.value != 0:
            print(docname, " already in ES!")
            return

        # article.author = author
        article.subject = "Python_2_sem"

        article.text = text
        article.tokens = tokens
        article.tokenstring = tokenstring
        article.fingerprints = fingerprints
        article.fpp = fp
        article.save()

        es.indices.refresh(index=NAME_IN)
        print(filename, " added!")

    @staticmethod
    def PAPA(filename, source):
        """
            Проверка кода с базой
        """

        with open(filename, 'r', encoding='utf-8') as f:
            text = f.readlines()

        tokens = token.tokenizer(filename, TOKENS_JSON)
        tokenstring = ''.join([x[0] for x in tokens])
        fingerprints = finger.fingerprints(tokens, K, T)
        fp = finger.ngramss(tokenstring, 5)

        buf = filename.split('/')
        docname = filename
        buf = docname.split('/')
        # if len(buf) != 4:
        #     print("Filename error")
        #     exit()
        # author = buf[3]

        article = Article()
        article.index_name = NAME_IN
        article.docname = docname
        article.text = text
        article.tokens = tokens
        # article.author = author
        article.tokenstring = tokenstring
        article.fingerprints = fingerprints
        article.fpp = fp

        new_hash_fingerprints = list(x[0] for x in fingerprints)

        if source.lower() == 'all':
            result = Search(using=es, index=NAME_IN, ) \
                .query("match", index_name=NAME_IN)
        # else:
        #     buf = source.split('_')
        #     if (len(buf)) == 1:
        #         result = Search(using=es, index=NAME_IN) \
        #             .query("match", subject=buf[0])
        #     elif (len(buf)) == 2:
        #         result = Search(using=es, index=NAME_IN) \
        #             .query("match", subject=buf[0]) \
        #             .query("match", work_type=buf[1])
        #     elif (len(buf)) == 3:
        #         result = Search(using=es, index=NAME_IN) \
        #             .query("match", subject=buf[0]) \
        #             .query("match", work_type=buf[1]) \
        #             .query("match", task_num=buf[2])
        #         print(buf)
        #     else:
        #         print("Incorrect source!")
        #         exit()

        es.indices.refresh(index=NAME_IN)

        response = result.execute()
        reports = list()

        if response.hits.total.value == 0:
            print(" ES have no docs to compare!")
            return False
        else:
            for hit in result.scan():
                # if hit.author == author:
                #     continue
                if hit.docname == docname:
                    continue
                a = fuzz.ratio(tokenstring, hit.tokenstring)
                old_fp = list(x[0] for x in hit.fpp)
                similarity = finger.jaccard_similarity(fp, hit.fpp)
                old_hash_fingerprints = list(x[0] for x in hit.fingerprints)
                intersection = list(set(new_hash_fingerprints).intersection(set(old_hash_fingerprints)))
                token_distance = len(intersection) / min(len(set(old_hash_fingerprints)),
                                                         len(set(new_hash_fingerprints))) * 100
                reports.append(
                    (a, token_distance, finger.report(fingerprints, hit.fingerprints), hit.docname, similarity))

                # except TypeError as e:
                #     print(f"У этого студента мало кода {hit.docname}")
                # finally:
                #     pass

        reports.sort(key=lambda x: x[0], reverse=True)
        results = []
        for report in reports[:6]:
            if ((report[0] >= 85) or (report[1] >= 35)) or (report[4] >= 80):
                resultt = (docname, report[1], report[0], report[3], report[4])
                results.append(resultt)
                fingerprint.write_to_csv(results, "./check/plagiarism.csv",
                                         ["совпавший файл", "процент по отпечаткам", "процент по Левенштейну",
                                          "коэффициент Жаккара",
                                          "файл в базе"])

                print("Сходство ", docname, " с документом ", report[3], " по Левенштейну - ", report[0],
                      "%, по отпечаткам - ", report[1], "%.", "По коэффициенту Жаккара", report[4])
                finger.print_report(report[2], docname, report[3])
                print()

        return False


elastic = Start()


@click.group()
def main():
    @main.command(name='create', help='Create index')
    def help():
        print("Справка")


@main.command(name='create', help='Create index')
def create_click():
    elastic.create()


@main.command(name='add', help='Load doc from file. Params: -f [path to file]')
@click.option('-f', '--filename', type=click.Path(exists=True, file_okay=True, dir_okay=False), required=True,
              prompt=True, help='Filename/path')
def add_click(filename):
    elastic.add(filename)


@main.command(name='add_dir', help='Load docs from dir. Params: -p [path to dir (without dirs) with files')
@click.option('-p', '--path', type=click.Path(exists=True, file_okay=False, dir_okay=True), required=True, prompt=True,
              help='Path to dir')
def add_dir_click(path):
    for filename in os.listdir(path):
        if filename.endswith(".py") and "test_json_analytics_prepod.py" != filename:
            elastic.add(path + '/' + filename)
        else:
            for file in os.listdir(path + "/" + filename):
                if file.endswith(".py") and "test_json_analytics_prepod.py" != file:
                    elastic.add(path + '/' + filename + "/" + file)
    # else:
    #     for filename in os.listdir(path):
    #         if filename.endswith(".py"):
    #             elastic.add(path + "/" + filename)


@main.command(name='PAPA_dir',
              help='PAPA-ing docs from dir. Params: -p [path to dir (without dirs) with files to check] -s [source in ES]')
@click.option('-p', '--path', type=click.Path(exists=True, file_okay=False, dir_okay=True), required=True, prompt=True,
              help='Path to dir')
@click.option('-s', '--source', required=True, prompt=True, help='Source')
def PAPA_dir_click(path, source):
    for filename in os.listdir(path):
        if filename.endswith(".py"):
            if elastic.PAPA(path + "/" + filename, source) == False and "test_json_analytics_prepod.py" != filename:
                print("Случай плагиата не зафиксирован, документ сохранён, см. отчёт в консоли")
                elastic.add(path + '/' + filename)
            else:
                print("Обнаружен плагиат, см. отчёт в консоли")

        else:
            for file in os.listdir(path + "/" + filename):
                if file.endswith(".py") and "test_json_analytics_prepod.py" != file.split("/")[-1]:
                    if elastic.PAPA(path + '/' + filename + "/" + file, source) == False:
                        print("Случай плагиата не зафиксирован, документ сохранён, см. отчёт в консоли")
                        elastic.add(path + '/' + filename + "/" + file)
                    else:
                        print("Обнаружен плагиат, см. отчёт в консоли")

    return 0


@main.command(name='PAPA', help='PAPA. Params: -f [path to file to check] -s [source in ES]')
@click.option('-f', '--filename', type=click.Path(exists=True, file_okay=True, dir_okay=False), required=True,
              prompt=True, help='Filename/path')
@click.option('-s', '--source', required=True, prompt=True, help='Source')
def PAPA_click(filename, source):
    if elastic.PAPA(filename, source) == False:
        print("Случай плагиата не зафиксирован, документ сохранён, см. отчёт в консоли")
        elastic.add(filename)
    else:
        print("Обнаружен плагиат, см. отчёт в консоли")
        return 1
    return 0


if __name__ == "__main__":
    # find_python_files("./Source")
    # remove_non_python_files("./data/2022-3-12-siv")
    main()
    # print(get_max_depth("./dataset/2022-3-kr1-v16-final/2022-3-01-and"))
    # token = token.tokenizer("./dataset/2022-3-kr1-v16-final/2022-3-11-pot/json_analytics.py", "./tokens/tokens_Python.json")
    # tokenstring = ''.join([x[0] for x in token])
    # #
    # token = token.tokenizer("./data/2022-3-11-pot-1/json_analytics.py", "./tokens/tokens_Python.json")
    # tokenstring1 = ''.join([x[0] for x in token])
    # # fingerprints = finger.fingerprints(token, K, T)
    # # print(fingerprints)
    # print(tokenstring1)
    # path = "./dataset/2022-3-kr1-v16-final"
    # for filename in os.listdir(path):
    #     for file in os.listdir(path + "/" + filename):
    #         if file.endswith(".py"):
    #             print(filename + '/' + file)

    # with open('./data/fastapi-todo-lr1', 'r') as readfile:
    #     s = readfile.read()
    #     for f in readfile:
    #         if f.startswith("import"):
    #             f = ''
    #         # print(f)
    # print(Sample('./data/fastapi-todo-lr1'))
    # # print(delete_comments(s))
    # # print(find_variables(s))
