import sqlite3


def create_db():
    conn = sqlite3.connect('db/plagiarism_check.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS files
                 (Text TEXT ,tokens TEXT, tokenstring TEXT,fingerprints TEXT , UNIQUE(Text, tokens))''')
    conn.commit()
    conn.close()


if __name__ == "__main__":
    create_db()
