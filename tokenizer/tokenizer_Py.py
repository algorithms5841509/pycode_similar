import re
import json


def tokenizer(filesource, filetokens):
    def remove_comments(code):
        pattern = r'""".*?"""|\'(?:\\.|[^\'\\n])*\'|"(?:\\.|[^"\\n])*"|/\*.*?\*/|#.*|\s+$'
        lines = re.sub(pattern, '', code, flags=re.MULTILINE | re.DOTALL)

        return lines.strip()

    def remove_comments_from_file(file_path):
        with open(file_path, 'r') as file:
            code = file.read()

        clean_code = remove_comments(code)

        with open(file_path, 'w') as file:
            file.write(clean_code)

    # remove_comments_from_file(filesource)
    SPECIALS = ["T", "C", "D", "B", "E", "A", "L", "K", "I", "S", "F", "V"]

    with open(filetokens, 'r', encoding='utf-8') as ft:
        TOKENS = json.loads(ft.read())

    with open(filesource, 'r', encoding='utf-8') as f:
        data_begin = f.readlines()

    data = list(map(lambda x: x.strip(), data_begin))
    #
    # # Чистка числовых констант
    # data = list(map(lambda d: re.sub(r"\-?[\d]+[\.\d]*", '', d), data))

    # for i in range(len(data)):
    #     for ph in re.findall(r'def\s+(\w+)\s*', data[i]):
    #         data[i] = data[i].replace(ph, '')[:3]
    # for i in range(len(data)):
    #     for ph in re.findall(r'class\s+(\w+)\s*', data[i]):
    #         data[i] = data[i].replace(ph, '')[:5]
    # Чистка строчных комментариев
    for i in range(len(data)):
        for ph in re.findall(r'#.*?$|''(?:\'\'\'[\s\S]*?\'\'\'|"""[\s\S]*?""")', data[i]):
            data[i] = data[i].replace(ph, '')

    for i in range(len(data)):
        # Чистка подключаемых библиотек комментариев
        for ph in re.findall(
                r'(?:(?:(?:import|from)\s+[a-zA-Z_][a-zA-Z0-9_.]*\s+import.*)|(?:import\s+[a-zA-Z_][a-zA-Z0-9_]*\s*.))$',
                data[i]):
            data[i] = data[i].replace(ph, '')
        # # Чистка строчных аргументов
        # for ph in re.findall(r"[\"\']+.*[\"\']+", data[i]):
        #     data[i] = data[i].replace(ph, '')

           # Чистка строчных аргументов
    for i in range(len(data)):
        for ph in re.findall(r"[\"\']+.*[\"\']+", data[i]):
            data[i] = data[i].replace(ph, '')
    for k in TOKENS['TYPES']:
        data = [re.sub(r'\b{}\b'.format(k), 'T', d) for d in data]
    for k in TOKENS['CONTEXT_MANAGERS']:
        data = [re.sub(r'\b{}\b'.format(k), 'C', d) for d in data]
    for k in TOKENS['DATABASE']:
        data = [re.sub(r'\b{}\b'.format(k), 'D', d) for d in data]
    for k in TOKENS['EXCEPTIONS']:
        data = [re.sub(r'\b{}\b'.format(k), 'E', d) for d in data]
    for k in TOKENS['K_WORDS']:
        data = [re.sub(r'\b{}\b'.format(k), 'K', d) for d in data]
    for k in TOKENS['OPERATORS_SRVN']:
        data = list(map(lambda d: d.replace(k, 'S'), data))
    for k in TOKENS['OPERATORS_ARIFM']:
        data = list(map(lambda d: d.replace(k, 'A'), data))
    for k in TOKENS['DECIMAL']:
        data = list(map(lambda d: d.replace(k, 'H'), data))
    for k in TOKENS['OPERATORS_LOG']:
        data = [re.sub(r'\b{}\b'.format(k), 'L', d) for d in data]
    for k in TOKENS['BUILT_IN_APP']:
        data = [re.sub(r'\b{}\b'.format(k), 'I', d) for d in data]

    # Поиск методов и функций
    data = list(map(lambda d: re.sub(r'\.([\d\w\_]*)\(', 'F', d), data))
    data = list(map(lambda d: re.sub(r' ?([\d\w\_]+)\(', 'F', d), data))
    # Поиск переменных
    for i in range(len(data)):
        for v in re.findall(r'\b(?![TCDEALKISVF])[A-Za-z_][A-Za-z0-9_]*\b', data[i]):
            data[i] = data[i].replace(v, 'V')
        for x in re.findall(r'[TCDEALKISVF]+([A-z0-9\_]+)', data[i]):
            data[i] = data[i].replace(x, 'V')

    result = []
    for i in range(len(data)):
        for symbol in data[i]:
            if symbol in SPECIALS:
                result.append((symbol, i + 1))

    return result
