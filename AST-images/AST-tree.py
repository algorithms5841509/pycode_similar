import ast
import astpretty
import json
import os

import networkx as nx
import matplotlib.pyplot as plt


def get_all_function_nodes(ast_node):
    nodes = []
    if isinstance(ast_node, (ast.FunctionDef, ast.AsyncFunctionDef)):
        nodes.append(ast_node)
    elif isinstance(ast_node, list):
        for node in ast_node:
            nodes.extend(get_all_function_nodes(node))
    elif isinstance(ast_node, ast.AST):
        for field in ast.iter_fields(ast_node):
            value = getattr(ast_node, field[0])
            if isinstance(value, list):
                for item in value:
                    nodes.extend(get_all_function_nodes(item))
            elif isinstance(value, ast.AST):
                nodes.extend(get_all_function_nodes(value))
            elif isinstance(value, ast.alias):
                nodes.extend(get_all_function_nodes(value.name))
    return nodes



def get_node_name(ast_node):
    if isinstance(ast_node, (ast.FunctionDef, ast.AsyncFunctionDef)):
        return ast_node.name
    elif isinstance(ast_node, ast.ClassDef):
        return ast_node.name
    else:
        return None


def create_graph(project_directory):
    graph = nx.DiGraph()
    for root, dirs, files in os.walk(project_directory):
        for file in files:
            if file.endswith(".py"):
                file_path = os.path.join(root, file)
                with open(file_path, "r") as f:
                    source_code = f.read()
                ast_node = ast.parse(source_code)
                nodes = get_all_function_nodes(ast_node)
                for node in nodes:
                    node_name = get_node_name(node)
                    graph.add_node(node_name)
                    if isinstance(node, (ast.FunctionDef, ast.AsyncFunctionDef)):
                        for child_node in ast.iter_child_nodes(node):
                            child_node_name = get_node_name(child_node)
                            if child_node_name is not None:
                                graph.add_edge(node_name, child_node_name)
                    elif isinstance(node, (ast.Import, ast.ImportFrom)):
                        for alias in node.names:
                            imported_module = alias.name.split(".")[-1]
                            graph.add_edge(node_name, imported_module)
    return graph



def save_graph_to_file(graph, file_path):
    plt.figure(figsize=(32, 20), dpi=80)
    pos = nx.spring_layout(graph)
    nx.draw_networkx_nodes(graph, pos)
    nx.draw_networkx_edges(graph, pos)
    nx.draw_networkx_labels(graph, pos)
    plt.savefig(file_path)


if __name__ == "__main__":
    project_directory = "/home/vladimir/Desktop/2022-3-kr1-v16-final/2022-3-13-sla"
    graph = create_graph(project_directory)
    output_file_path = "output.png"
    save_graph_to_file(graph, output_file_path)
