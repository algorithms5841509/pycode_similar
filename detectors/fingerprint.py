import hashlib, itertools
from nltk import ngrams
import csv

def ngramss(tokens: str, n: int) -> set:
    return list(tuple(tokens[i:i + n]) for i in range(0, len(tokens) - n + 1))


def jaccard_similarity(set1: list, set2: list) -> float:
    set1_hashable = tuple(attr for attrlist in set1 for attr in attrlist)
    set2_hashable = tuple(attr for attrlist in set2 for attr in attrlist)
    intersection = len(set(set1_hashable) & set(set2_hashable))
    union = len(set(set1_hashable) | set(set2_hashable))
    return (intersection / union) * 100 if union != 0 else 0


def str_to_hash(str):
    sum = int(hashlib.sha1(''.join([t[0] for t in str]).encode("utf-8")).hexdigest(), 16) % (10 ** 8)
    return (sum, str[0][1], str[-1][1])
    # return (sum, int(str(sum)[0]), int(str(sum)[-1]))


def right_index(el, str):
    x = [i for i, ltr in enumerate(str) if ltr[0] == el[0]]
    return x[-1]

def finger_reload(data):
    data.sort(key=lambda x: x[0])

    res = list()
    buf = []
    if data:
        buf = list(data[0])
    else:
        return -1

    buf = list(data[0])

    for i in range(len(data)):
        if buf[0] == data[i][0]:
            if buf[1] == data[i][1] and buf[2] < data[i][2]:
                buf[2] = data[i][2]
            if buf[2] == data[i][1] or buf[2] == data[i][1] - 1:
                buf[2] = data[i][2]
            else:
                res.append(buf)
                buf = list(data[i])
        else:
            res.append(buf)
            buf = list(data[i])
    res.append(buf)

    result = list()
    for i in res:
        if i not in result:
            result.append(i)

    return result

def fingerprints(data: str, k: int, t: int):
    if not data:
        raise ValueError
    if len(data) < k:
        k = len(data)
    kgrams = ngrams(data, k)
    hashes = [str_to_hash(gram) for gram in kgrams]
    if len(hashes) < t:
        t = len(hashes)
    tHgrams = ngrams(hashes, t)
    fngrprnts = []
    for gram in tHgrams:
        min_hash = min(gram)
        if min_hash not in fngrprnts:
            fngrprnts.append(min_hash)
    return finger_reload(fngrprnts)


def report(data1, data2):
    result = []
    for x in data1:
        finder = list(filter(
            lambda x2: x2[0] == x[0],
            data2
        ))
        if finder:
            for r in finder:
                result.append((
                    x[0], x[1], x[2], r[1], r[2]
                ))

    result.sort(key=lambda x: x[0])

    res = list()
    for i in result:
        if i not in res:
            res.append(i)

    return res


def print_report(report, doc1, doc2):
    buf = list()
    if len(report) == 0:
        return
    report.sort(key=lambda x: x[1])

    for str in report:
        buf.append(list((str[1:5])))

    report.clear()

    for i in buf:
        if i not in report:
            report.append(i)

    buf.clear()
    bufl = list((report[0][0:2]))
    bufr = list()

    for item in report:
        if list(item[0:2]) == bufl:
            bufr.append(item[2:5])
        else:
            buf.append(list((bufl, bufr)))
            bufl = list(item[0:2])
            bufr = list()
            bufr.append(item[2:5])
    buf.append(list((bufl, bufr)))

    results = []
    for item in buf:
        item[1].sort(key=lambda x: x[0])
        if (item[0][1] - item[0][0]) > 1:
            out = []
            out.append(item[0][1])
            out.append(item[0][0])
            out_fin = tuple(out[::-1])
            if (len(doc2.split("/")) > 4):

                resultt = (doc1.split("/")[2] + "/" + doc1.split("/")[3], out_fin, item[1],
                       doc2.split("/")[3] + "/" + doc2.split("/")[4])
                results.append(resultt)
                print('Строки документа', doc1.split("/")[2] + "/" + doc1.split("/")[3], ' с номерами ', item[0][0],
                      ' - ',
                      item[0][1], ' похожи на строки ',
                      item[1],
                      ' документа ', doc2.split("/")[3] + "/" + doc2.split("/")[4])
            else:
                resultt = (doc1.split("/")[2] + "/" + doc1.split("/")[3], out_fin, item[1],
                       doc2.split("/")[2] + "/" + doc2.split("/")[3])

                results.append(resultt)
                print('Строки документа', doc1.split("/")[2] + "/" + doc1.split("/")[3], ' с номерами ', item[0][0], ' - ',
                      item[0][1], ' похожи на строки ',
                      item[1],
                      ' документа ', doc2.split("/")[2] + "/" + doc2.split("/")[3])

    result = []
    for b in buf:
        source = [x for x in range(b[0][0], b[0][1] + 1)]
        b[1] = [
            [s for s in range(x[0], x[1] + 1)]
            for x in b[1]
        ]

        similarList = []
        for p in itertools.permutations(b[1]):
            if (len(p) > 1 and (set(p[0]) & set(p[1]))):
                simData = list(set(p[0]).union(set(p[1])))
                simData.sort()

                if simData not in similarList:
                    similarList.append(simData)
            else:
                if p[0] not in similarList:
                    similarList.append(p[0])

                if len(p) > 1 and p[1] not in similarList:
                    similarList.append(p[1])

        tempPush = [
            source, similarList
        ]
        bulka = False
        for k in result:
            if (set(k[0]) & set(source)):
                k[0] = list(set(k[0]).union(set(source)))
                k[1] += similarList
                bulka = True

        if tempPush not in result and not bulka:
            result.append(tempPush)
    write_to_csv(results, f"./results/{doc1.split("/")[2] + "-" + doc2.split("/")[3]}.csv",
                ['doc1', 'строки', 'совпавшие строки', 'doc2'])
    return result


def write_to_csv(results, output_file, fieldnames):
    with open(output_file, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        if len(fieldnames) > 4:
            for result in results:
                row = {
                    fieldnames[0]: result[0],
                    fieldnames[1]: result[1],
                    # 'конец': result[1],
                    fieldnames[2]: result[2],
                    fieldnames[3]: result[4],
                    fieldnames[4]: result[3]
                }
                writer.writerow(row)
        else:
            for result in results:
                row = {
                    fieldnames[0]: result[0],
                    fieldnames[1]: result[1],
                    # 'конец': result[1],
                    fieldnames[2]: result[2],
                    fieldnames[3]: result[3]
                }
                writer.writerow(row)
